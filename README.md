# Project 5: Brevet time calculator with Ajax and MongoDB

Simple list of controle times from project 4 stored in MongoDB database.

## Project information:

Author: Jackson Klagge

Email: Jklagge1998@gmail.com

Project Goal: Integrate proj4 with MongoDB by storing the list of controle times given in project 4 into a MongoDB database.

## Functionality I added

In this project, I created the following functionality. 1) Created two buttons ("Submit") and ("Display") in the page that contains the controle times (calc.html). 2) When clicking the Submit button, the control times should be entered into the mongo database. 3) On clicking the Display button, the entries from the database should be displayed in a new page(Todo.html). 

## Project Specificiations & assumptions

As in the last project (proj4-brevets) I made the assumption that the controle times will be decided on a hard cutoff. For example if the user enters 200 km acp_times.py will calculate the open and close times according to the second, not first, column of the table. The table can be found in times_acp.py. It is also expected that the user will input inbetween the range of 0 and 1300 km. 

## Button behavior & expected outputs

When the user clicks on the display and submit buttons this is the behavior they should expect:

*  If the user presses the display button without entering any data they will be redirected to isempty.html, which will give a message about the database being empty and will redirect the user to submit something before displaying.

*  If the user presses the submit button they can expect it's working if the entries dissappear and calc.html (the brevet table) is refreshed with blank entries, regardless of whether the user inputed anything or not.

*  If the user presses the display button and there are entries to the database the program will redirect to a page (todo.html) which displays the current content of submitted table entries. 
