import os
from flask import Flask, redirect, url_for, request, render_template, jsonify
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY


client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
db.tododb.delete_many({}) #resets the database upon each use of the application

###
# Pages
###

@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return render_template('calc.html')

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    return render_template('404.html'), 404

@app.route("/isempty")
def empty():
    return render_template('isempty.html')

@app.route('/todo', methods=['POST'])
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]

    if items == []:
        return render_template('isempty.html')
    else:
        return render_template('todo.html', items=items)

@app.route('/new', methods=['POST'])
def new():
    #retrieves the fields once the submit button is pressed
    km = request.form.getlist('km')
    open_times = request.form.getlist('open')
    close_times = request.form.getlist('close')
    app.logger.info("All rows")
    app.logger.info("km_list= {}".format(km))
    app.logger.info("open_times_list= {}".format(open_times))
    app.logger.info("close_times_list= {}".format(close_times))
    # gets rid of blank submissions
    km = [x for x in km if x != ""]
    open_times = [x for x in open_times if x != ""]
    close_times = [x for x in close_times if x != ""]

    app.logger.info("Non Blank rows")
    app.logger.info("km_list= {}".format(km))
    app.logger.info("open_times_list= {}".format(open_times))
    app.logger.info("close_times_list= {}".format(close_times))
    #inputs our feilds into the database
    for i in range(len(km)):
        item_doc = {
            'km': km[i],
            'open_time': open_times[i],
            'close_time': close_times[i]
        }
        db.tododb.insert_one(item_doc)
    #we then retrieve the database to see if it's empty
    _items = db.tododb.find()
    items = [item for item in _items]

    #if the database is empty we redirect the user to the isempty.html page
    if items == []:
    	return redirect(url_for('empty'))
    #else we redirect the user 
    return redirect(url_for('index')) #maybe we don't want this. Kind of 
                                     #does displays job aswell

###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.info("km={}".format(km))
    app.logger.info("request.args: {}".format(request.args))
    brevet_dist_km = request.args.get('brevet_dist_km', type=str)
    app.logger.info("brevet_dist_km: {}".format(brevet_dist_km))
    begin_date = request.args.get('begin_date', type=str)
    app.logger.info("begin_date: {}".format(begin_date))
    begin_time = request.args.get('begin_time', type=str)
    app.logger.info("begin_time: {}".format(begin_time))
    start_time = "{} {}".format(begin_date, begin_time)
    app.logger.info("start_time before arrow: {}".format(start_time))
    start_time = arrow.get(start_time)
    app.logger.info("start_time after arrow: {}".format(start_time))

    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, brevet_dist_km, start_time)
    close_time = acp_times.close_time(km, brevet_dist_km, start_time)
    app.logger.info("open_time: {}".format(open_time))
    app.logger.info("close_time: {}".format(close_time))
    result = {"open": open_time, "close": close_time}
    return jsonify(result=result)


#############

#From brevets.py in proj4-brevets
app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
