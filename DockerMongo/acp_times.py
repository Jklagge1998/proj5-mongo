"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

control_location = [200, 400, 600, 1000, 1300]
minimum_speed = [15, 15, 15, 11.428, 13.333]
maximum_speed = [34, 32, 30, 28, 26]

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    col = 0
    for location in control_location:
        if control_dist_km < location:
   	        break
        elif col < len(control_location) - 1:
            col += 1
    open_hours = (control_dist_km // maximum_speed[col])
    open_minutes = round(((control_dist_km / maximum_speed[col]) - open_hours) * 60)
    return arrow.get(brevet_start_time).shift(hours=+open_hours, minutes=+open_minutes).isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    col = 0
    for location in control_location:
        if control_dist_km < location:
            break
        elif col < len(control_location) - 1:
            col += 1
    close_hours = (control_dist_km // minimum_speed[col])
    close_minutes = round(((control_dist_km / minimum_speed[col]) - close_hours) * 60)
    return arrow.get(brevet_start_time).shift(hours=+close_hours, minutes=+close_minutes).isoformat()
